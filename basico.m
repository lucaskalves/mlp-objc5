#include <Foundation/Foundation.h>
#include "ProtocoloDoVideoProjetor.h"


int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  //Coloque o código deste exercício a partir daqui

    //Conectar-se a maquina localhost e tentar obter um proxy para MeuServidorDeBackup
  id server = [NSConnection
         rootProxyForConnectionWithRegisteredName: @"DataShowPublisher"
               host: @"10.67.101.245"
          usingNameServer: [NSSocketPortNameServer sharedInstance]];

  //Setar qual eh o protocolo obedecido pelo objeto do servidor (opcional)
  [server setProtocolForProxy:@protocol(ProtocoloDoVideoProjetor)];

  //Enviar a mensagem backup (ou chamar o metodo backup) do objeto remoto
  [server publicaMensagem: @"hello..."];

  //Fim do programa
  NSLog(@"Work finished.");


  //Programa termina
  [pool release];
  return 0;
}
