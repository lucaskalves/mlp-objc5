#include <Foundation/Foundation.h>

@protocol BackupServerProtocol
- (void) backup;
@end
